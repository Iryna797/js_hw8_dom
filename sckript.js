//1
const paragraphs = document.querySelectorAll('p');

paragraphs.forEach(elem => elem.style.backgroundColor = "#ff0000");

//2
let element = document.getElementById("optionsList");
console.log(element);
console.log(element.parentElement);
element.hasChildNodes(); // true
element = document.getElementById("optionsList").childNodes;
for (let i of element) {
  console.log("Name: " + i + ", " + "Type: " + typeof i);
}

//3
const textElement = document.getElementById("testParagraph");
textElement.innerHTML = "This is a paragraph";

console.log(textElement);

//4
const liList = document.querySelectorAll('.main-header li');

console.log(liList);

for (let element of liList) {
    element.classList.add('nav-item');
  }

//5
const someElement = document.querySelectorAll('.section-title');

for (let el of someElement) {
    el.classList.remove("section-title");
    console.log(someElement);
}
