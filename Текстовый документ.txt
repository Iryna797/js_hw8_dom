Теоретичні питання
1.Опишіть своїми словами що таке Document Object Model (DOM)
DOM- це деревоподібне відтворення веб-сайту, у вигляді об'єктів, вкладенних один в одного.
Браузер створює DOM для того, щоб за допомогою JavaScript можна було швидко шукати потрібний елемент, 
додавати нові елементи та вносити зміні до цих елементів.


2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerText - те ж, що і innerHTML, проте не може містити HTML теги.

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
За допомогою пошукових методів:
    getElementById() - звернення до елементу за унікальнім id;
    getElementsByName() - пошук серед елементів з атрибутом name, повертає список всіх елементів, 
    чий атрибут name задовольняє запиту;
    getElementsByClassName() - звернення до всіх елементів з однаковою назвою классу;
    getElementsByTagName() - звернення до всіх елементів з однаковою назвою тегу;
    querySelector() - шукає та повертає перший елемент, що задовольняє даному CSS-селектору;
    querySelectorAll() - повертає всі елементи, що задовольняють даномму CSS-селектору.

    Частіше використовуються методи querySelector() та querySelectorAll(). 
